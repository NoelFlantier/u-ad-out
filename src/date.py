import pyotherside
import time
import os
import threading
import sys
sys.path.append('deps')
sys.path.append('../deps')
lib_path = os.path.join(os.path.abspath(os.path.dirname(os.path.dirname(__file__))),"python_lib",)
sys.path.append(lib_path)
import pexpect

class Date: 
    def lastupdate(self):
    
        os.chdir("/home/phablet")
               
        #Getting the last modification date
        print("Getting last update")
        file_time = os.path.getmtime('/etc/hosts')
        date = time.strftime('%d/%m/%Y, %H:%M:%S', time.localtime(file_time))
        print(date)
        pyotherside.send('whatDate', date)
        
        return ""          
date = Date()
