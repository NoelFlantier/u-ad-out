import pyotherside
import time
import os
import threading
import sys
sys.path.append('deps')
sys.path.append('../deps')
lib_path = os.path.join(os.path.abspath(os.path.dirname(os.path.dirname(__file__))),"python_lib",)
sys.path.append(lib_path)
import pexpect

class Installer:
    def StevenBlack(self,password):
        
        os.chdir("/home/phablet")
        
        #Starting bash and getting root privileges
        print("Starting bash and getting root privileges")
        pyotherside.send('whatState',"=> Starting installer...")
        time.sleep(1.5)
        child = pexpect.spawn('bash')
        child.expect(r'\$')
        child.sendline('sudo -s')
        child.expect('[p/P]ass.*')
        child.sendline(str(password))
        i = child.expect (['[s/S]orry.*', 'root.*'])
        if i==0:
            print("Password Error")
            pyotherside.send('whatState','🚫️ Wrong password, try again 🚫️')
            child.kill()
        elif i==1:
            print("remounting filesystem to rw")
            pyotherside.send('whatState',"=> remounting filesystem...")

        #remounting filesystem to rw
        time.sleep(1.5)
        child.sendline('sudo mount -o remount,rw /')
        child.expect('root.*')
        
        #importing beautiful soup library
        print("importing library")
        pyotherside.send('whatState',"=> importing library...")
        from bs4 import BeautifulSoup
        from urllib import request
        from urllib.request import Request, urlopen

        #Downloading the list
        print("Setting the right folder")
        pyotherside.send('whatState',"=> Setting the right folder...")
        child.sendline("cd /etc")
        child.expect('root.*')
        
        #Downloading Hosts file
        print("Downloading Hosts file")
        pyotherside.send('whatState',"=> Downloading Hosts file, please wait...")
        url = "https://raw.githubusercontent.com/StevenBlack/hosts/master/hosts"
        hdr = {'User-Agent': 'Mozilla/5.0'}
        req = Request(url,headers=hdr)
        gfg = BeautifulSoup(request.urlopen(req).read(),"html.parser")
        res = gfg.get_text()
        f = open("hosts", "w")
        print(res, file=f)
        f.close()
        
        #Moving the file to the right folder
        print("Moving the file to the right folder")
        pyotherside.send('whatState',"=> Moving the file to the right folder...")
        child.sendline("sudo mv /home/phablet/hosts /etc/")
        child.expect('root.*')
              
        #remounting filesystem to ro
        print("remounting filesystem to rw")
        pyotherside.send('whatState',"=> remounting filesystem...")
        time.sleep(1.5)
        child.sendline('sudo mount -o remount,ro /')
        child.expect('root.*')
        print("done, hosts list installed")        
        pyotherside.send('whatState',"✔️ Steven Black Unified is installed")              
        child.close()
        return ""
    
    def AdAway(self, password):
        os.chdir("/home/phablet")
        
        #Starting bash and getting root privileges
        print("Starting bash and getting root privileges")
        pyotherside.send('whatState',"=> Starting installer...")
        time.sleep(1.5)
        child = pexpect.spawn('bash')
        child.expect(r'\$')
        child.sendline('sudo -s')
        child.expect('[p/P]ass.*')
        child.sendline(str(password))
        i = child.expect (['[s/S]orry.*', 'root.*'])
        if i==0:
            print("Password Error")
            pyotherside.send('whatState','🚫️ Wrong password, try again 🚫️')
            child.kill()
        elif i==1:
            print("remounting filesystem to rw")
            pyotherside.send('whatState',"=> remounting filesystem...")

        #remounting filesystem to rw
        time.sleep(1.5)
        child.sendline('sudo mount -o remount,rw /')
        child.expect('root.*')

        #importing beautiful soup library
        print("importing library")
        pyotherside.send('whatState',"=> importing library...")
        from bs4 import BeautifulSoup
        from urllib import request
        from urllib.request import Request, urlopen

        #Downloading the list
        print("Setting the right folder")
        pyotherside.send('whatState',"=> Setting the right folder...")
        child.sendline("cd /etc")
        child.expect('root.*')
        
        #Downloading Hosts file
        print("Downloading Hosts file")
        pyotherside.send('whatState',"=> Downloading Hosts file, please wait...")
        url = "https://adaway.org/hosts.txt"
        hdr = {'User-Agent': 'Mozilla/5.0'}
        req = Request(url,headers=hdr)
        gfg = BeautifulSoup(request.urlopen(req).read(),"html.parser")
        res = gfg.get_text()
        f = open("hosts", "w")
        print(res, file=f)
        f.close()
        
        #Moving the file to the right folder
        print("Moving the file to the right folder")
        pyotherside.send('whatState',"=> Moving the file to the right folder...")
        child.sendline("sudo mv /home/phablet/hosts /etc/")
        child.expect('root.*')

              
        #remounting filesystem to rw
        print("remounting filesystem to rw")
        pyotherside.send('whatState',"=> remounting filesystem...")
        time.sleep(1.5)
        child.sendline('sudo mount -o remount,ro /')
        child.expect('root.*')
        print("done, hosts list installed")        
        pyotherside.send('whatState',"✔️ AdAway is installed")
        child.close()
        return ""
        
    def StevenBlackPorn(self, password):
        os.chdir("/home/phablet")
        
        #Starting bash and getting root privileges
        print("Starting bash and getting root privileges")
        pyotherside.send('whatState',"=> Starting installer...")
        time.sleep(1.5)
        child = pexpect.spawn('bash')
        child.expect(r'\$')
        child.sendline('sudo -s')
        child.expect('[p/P]ass.*')
        child.sendline(str(password))
        i = child.expect (['[s/S]orry.*', 'root.*'])
        if i==0:
            print("Password Error")
            pyotherside.send('whatState','🚫️ Wrong password, try again 🚫️')
            child.kill()
        elif i==1:
            print("remounting filesystem to rw")
            pyotherside.send('whatState',"=> remounting filesystem...")

        #remounting filesystem to rw
        time.sleep(1.5)
        child.sendline('sudo mount -o remount,rw /')
        child.expect('root.*')
        
        #importing beautiful soup library
        print("importing library")
        pyotherside.send('whatState',"=> importing library...")
        from bs4 import BeautifulSoup
        from urllib import request
        from urllib.request import Request, urlopen

        #Downloading the list
        print("Setting the right folder")
        pyotherside.send('whatState',"=> Setting the right folder...")
        child.sendline("cd /etc")
        child.expect('root.*')
        
        #Downloading Hosts file
        print("Downloading Hosts file")
        pyotherside.send('whatState',"=> Downloading Hosts file, please wait...")
        url = "https://raw.githubusercontent.com/StevenBlack/hosts/master/alternates/porn/hosts"
        hdr = {'User-Agent': 'Mozilla/5.0'}
        req = Request(url,headers=hdr)
        gfg = BeautifulSoup(request.urlopen(req).read(),"html.parser")
        res = gfg.get_text()
        f = open("hosts", "w")
        print(res, file=f)
        f.close()
        
        #Moving the file to the right folder
        print("Moving the file to the right folder")
        pyotherside.send('whatState',"=> Moving the file to the right folder...")
        child.sendline("sudo mv /home/phablet/hosts /etc/")
        child.expect('root.*')

              
        #remounting filesystem to rw
        print("remounting filesystem to rw")
        pyotherside.send('whatState',"=> remounting filesystem...")
        time.sleep(1.5)
        child.sendline('sudo mount -o remount,ro /')
        child.expect('root.*')
        print("done, hosts list installed")        
        pyotherside.send('whatState',"✔️ Steven Black + Porn is installed")
        child.close()
        return ""
        
    def EnergizedSpark(self, password):
        os.chdir("/home/phablet")
        
        #Starting bash and getting root privileges
        print("Starting bash and getting root privileges")
        pyotherside.send('whatState',"=> Starting installer...")
        time.sleep(1.5)
        child = pexpect.spawn('bash')
        child.expect(r'\$')
        child.sendline('sudo -s')
        child.expect('[p/P]ass.*')
        child.sendline(str(password))
        i = child.expect (['[s/S]orry.*', 'root.*'])
        if i==0:
            print("Password Error")
            pyotherside.send('whatState','🚫️ Wrong password, try again 🚫️')
            child.kill()
        elif i==1:
            print("remounting filesystem to rw")
            pyotherside.send('whatState',"=> remounting filesystem...")

        #remounting filesystem to rw
        time.sleep(1.5)
        child.sendline('sudo mount -o remount,rw /')
        child.expect('root.*')
        
        #importing beautiful soup library
        print("importing library")
        pyotherside.send('whatState',"=> importing library...")
        from bs4 import BeautifulSoup
        from urllib import request
        from urllib.request import Request, urlopen

        #Downloading the list
        print("Setting the right folder")
        pyotherside.send('whatState',"=> Setting the right folder...")
        child.sendline("cd /etc")
        child.expect('root.*')
        
        #Downloading Hosts file
        print("Downloading Hosts file")
        pyotherside.send('whatState',"=> Downloading Hosts file, please wait...")
        url = "https://block.energized.pro/spark/formats/hosts.txt"
        hdr = {'User-Agent': 'Mozilla/5.0'}
        req = Request(url,headers=hdr)
        gfg = BeautifulSoup(request.urlopen(req).read(),"html.parser")
        res = gfg.get_text()
        f = open("hosts", "w")
        print(res, file=f)
        f.close()
        
        #Moving the file to the right folder
        print("Moving the file to the right folder")
        pyotherside.send('whatState',"=> Moving the file to the right folder...")
        child.sendline("sudo mv /home/phablet/hosts /etc/")
        child.expect('root.*')

              
        #remounting filesystem to rw
        print("remounting filesystem to rw")
        pyotherside.send('whatState',"=> remounting filesystem...")
        time.sleep(1.5)
        child.sendline('sudo mount -o remount,ro /')
        child.expect('root.*')
        print("done, hosts list installed")        
        pyotherside.send('whatState',"✔️ Energized Spark is installed")
        child.close()
        return ""
        
    def EnergizedBasic(self, password):
        os.chdir("/home/phablet")
        
        #Starting bash and getting root privileges
        print("Starting bash and getting root privileges")
        pyotherside.send('whatState',"=> Starting installer...")
        time.sleep(1.5)
        child = pexpect.spawn('bash')
        child.expect(r'\$')
        child.sendline('sudo -s')
        child.expect('[p/P]ass.*')
        child.sendline(str(password))
        i = child.expect (['[s/S]orry.*', 'root.*'])
        if i==0:
            print("Password Error")
            pyotherside.send('whatState','🚫️ Wrong password, try again 🚫️')
            child.kill()
        elif i==1:
            print("remounting filesystem to rw")
            pyotherside.send('whatState',"=> remounting filesystem...")

        #remounting filesystem to rw
        time.sleep(1.5)
        child.sendline('sudo mount -o remount,rw /')
        child.expect('root.*')
        
        #importing beautiful soup library
        print("importing library")
        pyotherside.send('whatState',"=> importing library...")
        from bs4 import BeautifulSoup
        from urllib import request
        from urllib.request import Request, urlopen

        #Downloading the list
        print("Setting the right folder")
        pyotherside.send('whatState',"=> Setting the right folder...")
        child.sendline("cd /etc")
        child.expect('root.*')
        
        #Downloading Hosts file
        print("Downloading Hosts file")
        pyotherside.send('whatState',"=> Downloading Hosts file, please wait...")
        url = "https://block.energized.pro/basic/formats/hosts.txt"
        hdr = {'User-Agent': 'Mozilla/5.0'}
        req = Request(url,headers=hdr)
        gfg = BeautifulSoup(request.urlopen(req).read(),"html.parser")
        res = gfg.get_text()
        f = open("hosts", "w")
        print(res, file=f)
        f.close()
        
        #Moving the file to the right folder
        print("Moving the file to the right folder")
        pyotherside.send('whatState',"=> Moving the file to the right folder...")
        child.sendline("sudo mv /home/phablet/hosts /etc/")
        child.expect('root.*')

              
        #remounting filesystem to rw
        print("remounting filesystem to rw")
        pyotherside.send('whatState',"=> remounting filesystem...")
        time.sleep(1.5)
        child.sendline('sudo mount -o remount,ro /')
        child.expect('root.*')
        print("done, hosts list installed")        
        pyotherside.send('whatState',"✔️ Energized Basic is installed")
        child.close()
        return ""     
        
    def EnergizedUltimate(self, password):
        os.chdir("/home/phablet")
        
        #Starting bash and getting root privileges
        print("Starting bash and getting root privileges")
        pyotherside.send('whatState',"=> Starting installer...")
        time.sleep(1.5)
        child = pexpect.spawn('bash')
        child.expect(r'\$')
        child.sendline('sudo -s')
        child.expect('[p/P]ass.*')
        child.sendline(str(password))
        i = child.expect (['[s/S]orry.*', 'root.*'])
        if i==0:
            print("Password Error")
            pyotherside.send('whatState','🚫️ Wrong password, try again 🚫️')
            child.kill()
        elif i==1:
            print("remounting filesystem to rw")
            pyotherside.send('whatState',"=> remounting filesystem...")

        #remounting filesystem to rw
        time.sleep(1.5)
        child.sendline('sudo mount -o remount,rw /')
        child.expect('root.*')
        
        #importing beautiful soup library
        print("importing library")
        pyotherside.send('whatState',"=> importing library...")
        from bs4 import BeautifulSoup
        from urllib import request
        from urllib.request import Request, urlopen

        #Downloading the list
        print("Setting the right folder")
        pyotherside.send('whatState',"=> Setting the right folder...")
        child.sendline("cd /etc")
        child.expect('root.*')
        
        #Downloading Hosts file
        print("Downloading Hosts file")
        pyotherside.send('whatState',"=> Downloading Hosts file, please wait...")
        url = "https://block.energized.pro/ultimate/formats/hosts.txt"
        hdr = {'User-Agent': 'Mozilla/5.0'}
        req = Request(url,headers=hdr)
        gfg = BeautifulSoup(request.urlopen(req).read(),"html.parser")
        res = gfg.get_text()
        f = open("hosts", "w")
        print(res, file=f)
        f.close()
        
        #Moving the file to the right folder
        print("Moving the file to the right folder")
        pyotherside.send('whatState',"=> Moving the file to the right folder...")
        child.sendline("sudo mv /home/phablet/hosts /etc/")
        child.expect('root.*')

              
        #remounting filesystem to rw
        print("remounting filesystem to rw")
        pyotherside.send('whatState',"=> remounting filesystem...")
        time.sleep(1.5)
        child.sendline('sudo mount -o remount,ro /')
        child.expect('root.*')
        print("done, hosts list installed")        
        pyotherside.send('whatState',"✔️ Energized Ultimate is installed")
        child.close()
        return ""

    def EnergizedUnified(self, password):
        os.chdir("/home/phablet")
        
        #Starting bash and getting root privileges
        print("Starting bash and getting root privileges")
        pyotherside.send('whatState',"=> Starting installer...")
        time.sleep(1.5)
        child = pexpect.spawn('bash')
        child.expect(r'\$')
        child.sendline('sudo -s')
        child.expect('[p/P]ass.*')
        child.sendline(str(password))
        i = child.expect (['[s/S]orry.*', 'root.*'])
        if i==0:
            print("Password Error")
            pyotherside.send('whatState','🚫️ Wrong password, try again 🚫️')
            child.kill()
        elif i==1:
            print("remounting filesystem to rw")
            pyotherside.send('whatState',"=> remounting filesystem...")

        #remounting filesystem to rw
        time.sleep(1.5)
        child.sendline('sudo mount -o remount,rw /')
        child.expect('root.*')
        
        #importing beautiful soup library
        print("importing library")
        pyotherside.send('whatState',"=> importing library...")
        from bs4 import BeautifulSoup
        from urllib import request
        from urllib.request import Request, urlopen

        #Downloading the list
        print("Setting the right folder")
        pyotherside.send('whatState',"=> Setting the right folder...")
        child.sendline("cd /etc")
        child.expect('root.*')
        
        #Downloading Hosts file
        print("Downloading Hosts file")
        pyotherside.send('whatState',"=> Downloading Hosts file, please wait...")
        url = "https://block.energized.pro/unified/formats/hosts.txt"
        hdr = {'User-Agent': 'Mozilla/5.0'}
        req = Request(url,headers=hdr)
        gfg = BeautifulSoup(request.urlopen(req).read(),"html.parser")
        res = gfg.get_text()
        f = open("hosts", "w")
        print(res, file=f)
        f.close()
        
        #Moving the file to the right folder
        print("Moving the file to the right folder")
        pyotherside.send('whatState',"=> Moving the file to the right folder...")
        child.sendline("sudo mv /home/phablet/hosts /etc/")
        child.expect('root.*')
              
        #remounting filesystem to rw
        print("remounting filesystem to rw")
        pyotherside.send('whatState',"=> remounting filesystem...")
        time.sleep(1.5)
        child.sendline('sudo mount -o remount,ro /')
        child.expect('root.*')
        print("done, hosts list installed")        
        pyotherside.send('whatState',"✔️ Energized Unified is installed")
        child.close()
        return ""  
      
installer = Installer()
