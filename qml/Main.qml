/*
 * Copyright (C) 2022  Antoine Deslandes
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 3.
 *
 * uAdOut is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import QtQuick 2.7
import Ubuntu.Components 1.3
//import QtQuick.Controls 2.2
import QtQuick.Layouts 1.3
import Qt.labs.settings 1.0
import io.thp.pyotherside 1.3
import Ubuntu.Components.Popups 1.3
import "modules"

MainView {
    id: root
    objectName: 'mainView'
    applicationName: 'u-adout.noelflantier'
    automaticOrientation: true

    width: units.gu(45)
    height: units.gu(75)

    Component {
        id: warning
        Dialog {
            id: dialogue_warning
            title: i18n.tr("Warning")
            text: i18n.tr("Please note that this app will temporarily make filesystem read-write. Make sure you have a stable internet connexion before downloading hosts lists.")

            Timer {
                id: enterDelayTimer
                interval: 1000
                running: false
                onTriggered: entry.text = ""
            }
            Python {
                id: pythonPath
                Component.onCompleted: {
        	     addImportPath(Qt.resolvedUrl('../src/'));
                    importNames('installer', ['installer'], function() {
                        console.log('installer module imported');

                    });        	     
        	     importNames('dlstatus', ['dlstatus'], function() {
                        console.log('dlstatus module imported');
                    });                       
                    importNames('date', ['date'], function() {
                        console.log('date module imported');
                    });                                                                               
                }
            }  
            Button {
                text: i18n.tr("Ok")
                color: theme.palette.normal.positive

                enabled: !enterDelayTimer.running
                onClicked: {
                    PopupUtils.close(dialogue_warning)
                        python.call('dlstatus.dlstatus', function(returnValue) {
                            console.log('dlstatus called');
                        })                  
                        python2.call('date.lastupdate', function(returnValue) {
                    	     console.log('date called');
                        })  
                }
            }
            Button {
                text: i18n.tr("Cancel")
                enabled: !enterDelayTimer.running
                onClicked: {
                    PopupUtils.close(dialogue_warning)
                    Qt.quit()
                }
            }
        }
    }        

    PageStack {
      id: pageStack
      Component.onCompleted: push(page0)
      Page {
        id: page0
        visible: false
        anchors.fill: parent
        header: PageHeader {
            id: header0
            title: i18n.tr("uAdOut")
      trailingActionBar {
        actions: [
        Action {
            iconName: "info"
            text: i18n.tr("About")
            onTriggered: pageStack.push(Qt.resolvedUrl("About.qml"))
        },
        Action {
            iconName: "like"
            text: i18n.tr("Donate")
            onTriggered: Qt.openUrlExternally("https://ubports.com/fr/donate")        
        },
        Action {
            iconName: "dialog-question-symbolic"
            text: i18n.tr("Help")
            onTriggered: pageStack.push(Qt.resolvedUrl("Help.qml"))
        }
      ]
    }
  }

        Column {
            anchors.top: header0.bottom
            anchors.bottom: parent.bottom
            anchors.left: parent.left
            anchors.right: parent.right
            ListItem {
                Label {
                    text: i18n.tr("Steven Black Unified list")
                    anchors.centerIn: parent
                    font.pointSize: 35
                    wrapMode: Text.WordWrap
                }
                onClicked: {
                    pageStack.push(Qt.resolvedUrl("StevenBlackUnified.qml"))
                }
            }
            ListItem {
                  Label {
                      text: i18n.tr("Steven Black + Porn list")
                      anchors.centerIn: parent
                      font.pointSize: 35
                      wrapMode: Text.WordWrap
                  }
                  onClicked: {
                      pageStack.push(Qt.resolvedUrl("StevenBlackPorn.qml"))
                  }
            }
            ListItem {
                  Label {
                      text: i18n.tr("AdAway list")
                      anchors.centerIn: parent
                      font.pointSize: 35
                      wrapMode: Text.WordWrap
                  }
                  onClicked: {
                      pageStack.push(Qt.resolvedUrl("AdAway.qml"))
                  }
            }
            ListItem {
                  Label {
                      text: i18n.tr("Energized Spark list")
                      anchors.centerIn: parent
                      font.pointSize: 35
                      wrapMode: Text.WordWrap
                  }
                  onClicked: {
                      pageStack.push(Qt.resolvedUrl("EnergizedSpark.qml"))
                  }
		}
            ListItem {
                  Label {
                      text: i18n.tr("Energized Basic list")
                      anchors.centerIn: parent
                      font.pointSize: 35
                      wrapMode: Text.WordWrap
                  }
                  onClicked: {
                      pageStack.push(Qt.resolvedUrl("EnergizedBasic.qml"))
                  }
            }
            ListItem {
                  Label {
                      text: i18n.tr("Energized Utlimate list")
                      anchors.centerIn: parent
                      font.pointSize: 35
                      wrapMode: Text.WordWrap
                  }
                  onClicked: {
                      pageStack.push(Qt.resolvedUrl("EnergizedUltimate.qml"))
                  }
             }
             ListItem {
                  Label {
                      text: i18n.tr("Energized Unified list")
                      anchors.centerIn: parent
                      font.pointSize: 35
                      wrapMode: Text.WordWrap
                  }
                  onClicked: {
                      pageStack.push(Qt.resolvedUrl("EnergizedUnified.qml"))
                  }
             } 
             ListItem {
                  Label {
                      id: content
                      color: "gray"
                      text: i18n.tr(" ")
                      anchors.centerIn: parent
                      font.pointSize: 30
                      wrapMode: Text.WordWrap
                  }
             }
             ListItem {
                  Label {
                      id: lastupdate_title
                      color: "gray"
                      text: i18n.tr("Last update :")
                      anchors.centerIn: parent
                      font.pointSize: 30
                      wrapMode: Text.WordWrap
                  }
             }               
             ListItem {
                  Label {
                      id: lastupdate
                      color: "gray"
                      text: i18n.tr(" ")
                      anchors.centerIn: parent
                      font.pointSize: 30
                      wrapMode: Text.WordWrap
                  }
             }                                     
          }
        }
    Python {
        id: python
        Component.onCompleted: {
            python.setHandler('whatList',
                function (state) {
                    content.text = state
                } )                      
	}
	
        onError: {
            console.log('python error: ' + traceback);
        } 
    }
    Python {
        id: python2
        Component.onCompleted: {
            python.setHandler('whatDate',
                function (state) {
                    lastupdate.text = state
                } )                          
	}
	
        onError: {
            console.log('python error: ' + traceback);
        } 
    }   
    }
    Component.onCompleted: {
        PopupUtils.open(warning)
    }    
}
