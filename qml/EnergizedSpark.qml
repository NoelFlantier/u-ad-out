import QtQuick 2.7
import Ubuntu.Components 1.3
//import QtQuick.Controls 2.2
import QtQuick.Layouts 1.3
import Qt.labs.settings 1.0
import io.thp.pyotherside 1.3
import Ubuntu.Components.Popups 1.3

Page {
    id: aboutPage

    header: PageHeader {
        id: header
        title: i18n.tr("Back to menu - Energized Spark")
        opacity: 1
    }
    MainView {
        anchors.top: header.bottom
        anchors.left: parent.left
        anchors.right: parent.right
        anchors.bottom: parent.bottom
        ActivityIndicator {
            id: activity
            anchors.top: parent.top
            anchors.horizontalCenter: parent.horizontalCenter
            anchors.topMargin: parent.height / 15
        }
        Label {
            id: content
            anchors.top: (activity.running == true) ? activity.bottom : parent.top
            anchors.topMargin: (activity.running == true) ? parent.height / 15 : parent.height / 25
            anchors.horizontalCenter: parent.horizontalCenter
            width: parent.width / 1.5
            horizontalAlignment: Text.AlignHCenter
            text: i18n.tr("Press 'start' to download Energized's Spark hosts list.")
            font.pointSize: 25
            wrapMode: Text.Wrap
        }
        Button {
            id: startButton
            anchors.top: content.bottom
            anchors.topMargin: 10
            anchors.horizontalCenter: parent.horizontalCenter
            color: "green"
            text: i18n.tr("Start")
            onClicked: {
                startButton.visible = false
                startButtonFake.visible = true
                activity.running = true
                PopupUtils.open(passwordPrompt)
            }
        }
        Button {
            id: startButtonFake
            visible: false
            anchors.top: content.bottom
            anchors.topMargin: 10
            anchors.horizontalCenter: parent.horizontalCenter
            color: "gray"
            text: i18n.tr("Running")
            onClicked: console.log("Download is ongoing")
        }
        Button {
            id: doneButton
            visible: false
            anchors.top: content.bottom
            anchors.topMargin: 10
            anchors.horizontalCenter: parent.horizontalCenter
            color: "green"
            text: i18n.tr("Done, close the app")
            onClicked: Qt.quit()
        }          
    }

    Component {
        id: passwordPrompt
        Dialog {
            id: passPrompt
            title: i18n.tr("Password")
            Label {
                text: i18n.tr("Enter your password:")
                wrapMode: Text.Wrap
            }
            TextField {
                id: password
                placeholderText: i18n.tr("password")
                echoMode: TextInput.Password
            }

            Button {
                text: i18n.tr("Ok")
                color: "green"
                onClicked: {
                    PopupUtils.close(passPrompt)
                    python.call('installer.EnergizedSpark', [password.text], function(returnValue) {
                        console.log('installer called');
                    })
                    
                
                }

            }
            Button {
                text: i18n.tr("Cancel")
                color: "Gray"
                onClicked: {
                    PopupUtils.close(passPrompt)
                    startButton.visible = true
                    startButtonFake.visible = false
                    activity.running = false                      
                }
            }
        }
    }

    Python {
        id: python
        Component.onCompleted: {
            addImportPath(Qt.resolvedUrl('../src/'));

            python.setHandler('whatState',
                function (state) {
                    content.text = state
                    if (content.text == "✔️ Energized Spark is installed") {
                        activity.running = false
                        startButtonFake.visible = false
                        doneButton.visible = true
                        python.call('dlstatus.dlstatus', function(returnValue) {
                            console.log('dlstatus called');
                        })                                    
                        python2.call('date.lastupdate', function(returnValue) {
                    	     console.log('date called');
                        })                           
                    } else {
                        activity.running = true
                        startButtonFake.visible = true
                    }
                        if (content.text == "🚫️ Wrong password, try again 🚫️") {
                            activity.running = false
                            startButtonFake.visible = false
                            PopupUtils.open(passwordPrompt)
                        } else {
                            startButtonFake.visible = true
                        }
                })

	}
	
        onError: {
            console.log('python error: ' + traceback);
        }
    }
}
