import QtQuick 2.9
import Ubuntu.Components 1.3
//import QtQuick.Controls 2.2
import QtQuick.Layouts 1.3
import Qt.labs.settings 1.0
import io.thp.pyotherside 1.3
import Ubuntu.Components.Popups 1.3

Page {
    id: helpPage

    header: PageHeader {
        title: i18n.tr("Back to menu - Help")
    }

    ScrollView {
        id: scrollView
        anchors {
            top: helpPage.header.bottom
            bottom: parent.bottom
            left: parent.left
            right: parent.right
            leftMargin: units.gu(2)
            rightMargin: units.gu(2)
        }

        clip: true

        Column {
            id: aboutColumn
            spacing: units.gu(2)
            width: scrollView.width
            
            Label {
                anchors.horizontalCenter: parent.horizontalCenter
                wrapMode: Text.WrapAtWordBoundaryOrAnywhere
                text: i18n.tr("uAdOut")
                fontSize: "x-large"
            }

            UbuntuShape {
                width: units.gu(12); height: units.gu(12)
                anchors.horizontalCenter: parent.horizontalCenter
                radius: "medium"
                image: Image {
                    source: Qt.resolvedUrl("../assets/logo.png")
                }
            }

            Label {
                width: parent.width
                linkColor: UbuntuColors.orange
                horizontalAlignment: Text.AlignHCenter
                wrapMode: Text.WrapAtWordBoundaryOrAnywhere
                text: i18n.tr("Lists are ordered from most basic (Steven Black) to most complete (Energized Unified) protection. For example Energized Spark includes both Steven Black, AdAway's hosts and  hosts from other sources, Energized Ultimate includes the latter and hosts from other sources...")
            }
            Label {
                width: parent.width
                linkColor: UbuntuColors.orange
                horizontalAlignment: Text.AlignHCenter
                wrapMode: Text.WrapAtWordBoundaryOrAnywhere
                text: i18n.tr("If you cannot access a website and get the message 'Error: net::ERR_CONNECTION_REFUSED' you can try to downgrade protection by choosing another list higher in the app list. If you still see unwanted adds or webpages try going for another list lower in the app list.")
            }
            Label {
                width: parent.width
                linkColor: UbuntuColors.orange
                horizontalAlignment: Text.AlignHCenter
                wrapMode: Text.WrapAtWordBoundaryOrAnywhere
                text: i18n.tr("Please note that the process fetching the name of the hosts list installed on your device will only be reliable if you installed the hosts list using this app.")
            }                                
        }
    }   
}
